const allowedOrigins = [
  "https://social-media-app-0ltx.onrender.com",
  "http://localhost:3000",
];

module.exports = allowedOrigins;
