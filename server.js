require("dotenv").config();
const mongoose = require("mongoose");
const express = require("express");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const socketServer = require("./socketServer");
const corsOptions = require("./config/corsOptions");

const PORT = process.env.PORT || 5000;
const userRouter = require("./routes/userRouter");
const postRouter = require("./routes/postRouter");
const commentRouter = require("./routes/commentRouter");
const notifyRouter = require("./routes/notifyRouter");
const messageRouter = require("./routes/messageRouter");

const URL = process.env.MONGODB_URL;
// connect mongodb
const connectDB = async () => {
  try {
    await mongoose.connect(URL, { useNewUrlParser: true, useUnifiedTopology: true });
    console.log("Connected to mongoDB");
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

connectDB();

const app = express();

app.use(express.json());
app.use(cors(corsOptions));
app.use(cookieParser());

//Socket
const http = require("http").createServer(app);
const io = require("socket.io")(http, {
  cors: {
    origin: "*",
    credentials: true,
  },
});

io.on("connection", (socket) => {
  socketServer(socket);
});

//Routes
app.use("/api", userRouter);
app.use("/api", postRouter);
app.use("/api", commentRouter);
app.use("/api", notifyRouter);
app.use("/api", messageRouter);

http.listen(PORT, () => {
  console.log("Server is running on port", PORT);
});
