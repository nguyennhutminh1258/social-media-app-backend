const mongoose = require("mongoose");

const commentSchema = mongoose.Schema(
  {
    content: {
      type: String,
      required: true,
    },
    tag: {
      type: Object,
    },
    reply: {
      type: mongoose.Types.ObjectId,
    }, // ID người dùng phản hồi bình luận
    likes: [{ type: mongoose.Types.ObjectId, ref: "user" }], // Danh sách người dùng thích bình luận
    user: { type: mongoose.Types.ObjectId, ref: "user" }, // ID của người dùng bình luận
    idPost: { type: mongoose.Types.ObjectId }, // ID của post chứa comment
    idUserPost: { type: mongoose.Types.ObjectId }, // ID của người dùng tạo post
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("comment", commentSchema);
