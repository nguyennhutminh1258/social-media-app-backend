const mongoose = require("mongoose");

const messageSchema = mongoose.Schema(
  {
    conversation: { type: mongoose.Types.ObjectId, ref: "conversation" }, // ID của cuộc trò chuyện
    sender: { type: mongoose.Types.ObjectId, ref: "user" }, // ID người gửi tin nhắn
    recipient: { type: mongoose.Types.ObjectId, ref: "user" }, // ID người nhận tin nhắn
    text: {
      type: String,
      required: true,
    },
    media: {
      type: Array,
    }, // Danh sách ảnh đính kèm với tin nhắn
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("message", messageSchema);
