const mongoose = require("mongoose");

const conversationSchema = mongoose.Schema(
  {
    recipients: [{ type: mongoose.Types.ObjectId, ref: "user" }], // ID 2 người tham gia conversation
    text: {
      type: String,
      required: true,
    },
    media: {
      type: Array,
    }, // Danh sách ảnh đính kèm với tin nhắn
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("conversation", conversationSchema);
