let users = [];

const socketServer = (socket) => {
  // Connect - Disconnect
  socket.on("joinUser", (id) => {
    users.push({ id, socketId: socket.id });
    console.log(`User ${id} connected to server`);
  });

  socket.on("disconnect", () => {
    users = users.filter((user) => user.socketId !== socket.id);
  });

  // Like
  socket.on("likePost", (newPost) => {
    const ids = [...newPost.user.followers, newPost.user._id];
    const clients = users.filter((user) => ids.includes(user.id));
    if (clients.length > 0) {
      clients.forEach((client) => {
        socket.to(`${client.socketId}`).emit("likeToClient", newPost);
      });
    }
  });

  //unLike
  socket.on("unLikePost", (newPost) => {
    const ids = [...newPost.user.followers, newPost.user._id];
    const clients = users.filter((user) => ids.includes(user.id));
    if (clients.length > 0) {
      clients.forEach((client) => {
        socket.to(`${client.socketId}`).emit("unLikeToClient", newPost);
      });
    }
  });

  // create comment
  socket.on("createComment", (newPostAfter) => {
    const ids = [...newPostAfter.user.followers, newPostAfter.user._id];
    const clients = users.filter((user) => ids.includes(user.id));
    if (clients.length > 0) {
      clients.forEach((client) => {
        socket
          .to(`${client.socketId}`)
          .emit("createCommentToClient", newPostAfter);
      });
    }
  });

  // delete comment
  socket.on("deleteComment", (newPost) => {
    const ids = [...newPost.user.followers, newPost.user._id];
    const clients = users.filter((user) => ids.includes(user.id));
    if (clients.length > 0) {
      clients.forEach((client) => {
        socket.to(`${client.socketId}`).emit("deleteCommentToClient", newPost);
      });
    }
  });

  // follow
  socket.on("follow", (newUser) => {
    const user = users.find((user) => user.id === newUser._id);
    user && socket.to(`${user.socketId}`).emit("followToClient", newUser);
  });

  // unFollow
  socket.on("unFollow", (newUser) => {
    const user = users.find((user) => user.id === newUser._id);
    user && socket.to(`${user.socketId}`).emit("unFollowToClient", newUser);
  });

  //Notification
  socket.on("createNotify", (notify) => {
    const clients = users.filter((user) => notify.recipients.includes(user.id));
    if (clients.length > 0) {
      clients.forEach((client) => {
        socket.to(`${client.socketId}`).emit("createNotifyToClient", notify);
      });
    }
  });

  socket.on("removeNotify", (notify) => {
    const clients = users.filter((user) => notify.recipients.includes(user.id));
    if (clients.length > 0) {
      clients.forEach((client) => {
        socket.to(`${client.socketId}`).emit("removeNotifyToClient", notify);
      });
    }
  });

  //Message
  socket.on("addMessage", (message) => {
    const client = users.find((user) => user.id === message.recipient);
    client &&
      socket.to(`${client.socketId}`).emit("addMessageToClient", message);
  });

  //Message
  socket.on("deleteMessage", (message) => {
    const client = users.find((user) => user.id === message.recipient);
    client &&
      socket.to(`${client.socketId}`).emit("deleteMessageToClient", message);
  });
};

module.exports = socketServer;
