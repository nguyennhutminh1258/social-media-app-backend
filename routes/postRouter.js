const router = require("express").Router();
const postController = require("../controllers/postController");

router.post("/post", postController.createPost);

router.get("/post/:id", postController.getPosts);

router.get("/detail_post/:id", postController.getDetailPost);

router.get("/user_post/:id", postController.getUserPost);

router.patch("/post/:id", postController.updatePost);

router.patch("/post/:id/like", postController.likePost);

router.patch("/post/:id/unlike", postController.unLikePost);

router.delete("/post/:id", postController.deletePost);

module.exports = router;
