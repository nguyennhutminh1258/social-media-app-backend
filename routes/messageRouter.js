const router = require("express").Router();
const messageController = require("../controllers/messageController");

router.post("/message", messageController.createMessage);

router.get("/conversations/:id", messageController.getConversation);

router.get("/messages/:sender/:recipient", messageController.getMessages);

router.delete("/message/:id", messageController.deleteMessage);

module.exports = router;
