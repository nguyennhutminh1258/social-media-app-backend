const router = require("express").Router();
const notifyController = require("../controllers/notifyController");

router.post("/notify", notifyController.createNotify);

router.delete("/notify/:id", notifyController.removeNotify);

router.delete("/delete_all_notify/:id", notifyController.deleteAllNotifies);

router.patch("/is_read/:id", notifyController.isRead);

router.get("/notify/:id", notifyController.getNotifies);

module.exports = router;
