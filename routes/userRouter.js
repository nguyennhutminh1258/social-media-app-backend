const router = require("express").Router();
const userController = require("../controllers/userController");

router.post("/register", userController.register);

router.post("/login", userController.login);

router.post("/logout", userController.logout);

router.post("/refresh_token", userController.generateAccessToken);

router.get("/search", userController.searchUser);

router.get("/user/:id", userController.getUser);

router.get("/suggestionUser/:id", userController.suggestionUser);

router.patch("/user", userController.updateUser);

router.patch("/user_password", userController.updatePasswordUser);

router.patch("/follow", userController.follow);

router.patch("/unfollow", userController.unfollow);

module.exports = router;
