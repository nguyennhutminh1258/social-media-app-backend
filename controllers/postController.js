const Post = require("../models/postModel");
const Comment = require("../models/commentModel");
const User = require("../models/userModel");

const postController = {
  createPost: async (req, res) => {
    try {
      const { content, images, _id } = req.body;

      const newPost = new Post({
        content,
        images,
        user: _id,
      });

      await newPost.save();

      res.json({ msg: "Tạo bài viết thành công", newPost });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  getPosts: async (req, res) => {
    const skip = req.query.skip ? Number(req.query.skip) : 0;
    const limit = req.query.limit ? Number(req.query.limit) : 0;
    try {
      const user = await User.findById(req.params.id).select("-password");
      if (!user) return res.status(400).json({ msg: "Tài khoản không tồn tại." });

      // Lấy post
      const posts = await Post.find({ user: [...user.following, user._id] })
        .skip(skip)
        .limit(limit)
        .sort("-createdAt")
        // Lấy thông tin user (trừ mật khẩu) từ userID của user và likes`
        .populate("user likes", "name userName avatar followers")
        // Lấy thông tin user (trừ mật khẩu) từ userID của user và likes trong comments
        .populate({
          path: "comments",
          options: { sort: { createdAt: -1 } },
          populate: { path: "user likes", select: "-password" },
        });

      res.json({ msg: "Lấy bài viết thành công", posts });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  getDetailPost: async (req, res) => {
    try {
      // Lấy post
      const post = await Post.findById(req.params.id)
        // Lấy thông tin user (trừ mật khẩu) từ userID của user và likes`
        .populate("user likes", "name userName avatar followers")
        // Lấy thông tin user (trừ mật khẩu) từ userID của user và likes trong comments
        .populate({
          path: "comments",
          options: { sort: { createdAt: -1 } },
          populate: { path: "user likes", select: "-password" },
        });

      if (!post) return res.status(400).json({ msg: "Bài viết này không tồn tại." });

      res.json({ msg: "Lấy bài viết thành công", post });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  getUserPost: async (req, res) => {
    const skip = req.query.skip ? Number(req.query.skip) : 0;
    const limit = req.query.limit ? Number(req.query.limit) : 0;
    try {
      const posts = await Post.find({ user: req.params.id })
        .skip(skip)
        .limit(limit)
        .sort("-createdAt")
        .populate("user likes", "name userName avatar followers")
        // Lấy thông tin user (trừ mật khẩu) từ userID của user và likes trong comments
        .populate({
          path: "comments",
          options: { sort: { createdAt: -1 } },
          populate: { path: "user likes", select: "-password" },
        });

      res.json({ msg: "Lấy post user profile thành công.", posts });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  updatePost: async (req, res) => {
    try {
      const { content, images } = req.body;

      const post = await Post.findOneAndUpdate(
        { _id: req.params.id },
        {
          content,
          images,
        }
      )
        .populate("user likes", "name userName avatar")
        .populate({
          path: "comments",
          populate: { path: "user likes", select: "-password" },
        });

      res.json({ msg: "Sửa bài viết thành công", newPost: { ...post._doc, content, images } });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  likePost: async (req, res) => {
    try {
      const { idUser } = req.body;

      const post = await Post.find({ _id: req.params.id, likes: idUser });
      if (post.length !== 0) return res.status(400).json({ msg: "Bạn đã thích bài viết này rồi." });

      const like = await Post.findOneAndUpdate(
        { _id: req.params.id },
        {
          $push: { likes: idUser },
        },
        { new: true }
      );

      if (!like) return res.status(400).json({ msg: "Bài viết này không tồn tại." });

      res.json({ msg: "Bạn đã thích bài post này." });
    } catch (error) {
      c;
    }
  },

  unLikePost: async (req, res) => {
    try {
      const { idUser } = req.body;

      const like = await Post.findOneAndUpdate(
        { _id: req.params.id },
        {
          $pull: { likes: idUser },
        },
        { new: true }
      );

      if (!like) return res.status(400).json({ msg: "Bài viết này không tồn tại." });

      res.json({ msg: "Bạn đã bỏ thích bài post này." });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  deletePost: async (req, res) => {
    try {
      const { idUserPost } = req.body;
      const user = await User.findById(idUserPost).select("-password");
      const post = await Post.findOneAndDelete({ _id: req.params.id, user: idUserPost });

      await Comment.deleteMany({ _id: { $in: post.comments } });

      res.json({ msg: "Xóa bài viết thành công", post: { ...post, user: user } });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },
};

module.exports = postController;
