const User = require("../models/userModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const userController = {
  register: async (req, res) => {
    try {
      const { name, userName, email, password, gender } = req.body;

      let newUserName = userName.toLowerCase().replace(/ /g, "");
      const existedUserName = await User.findOne({ userName: newUserName });
      if (existedUserName) return res.status(400).json({ msg: "Tên tài khoản này đã tồn tại." });

      const existedEmail = await User.findOne({ email });
      if (existedEmail) return res.status(400).json({ msg: "Email này đã được đăng ký." });

      if (password.length < 6) return res.status(400).json({ msg: "Password phải chứa it nhất 6 kí tự." });

      const passwordHash = await bcrypt.hash(password, 12);

      const newUser = new User({ name, userName: newUserName, email, password: passwordHash, gender });

      const accessToken = createAccessToken({ id: newUser._id });
      const refreshToken = createRefreshToken({ id: newUser._id });

      // Create secure cookie with refresh token
      res.cookie("refreshtoken", refreshToken, {
        httpOnly: true, //accessible only by web server
        secure: true, //https
        sameSite: "None", //cross-site cookie
        maxAge: 30 * 7 * 24 * 60 * 60 * 1000,
      });

      await newUser.save();

      res.json({ msg: "Đăng kí tài khoản thành công", accessToken, user: { ...newUser._doc, password: "" } });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },

  login: async (req, res) => {
    try {
      const { email, password } = req.body;

      const user = await User.findOne({ email }).populate("followers following", "-password");

      if (!user) return res.status(400).json({ msg: "Tài khoản này không tồn tại." });

      const isMatchPassword = await bcrypt.compare(password, user.password);
      if (!isMatchPassword) return res.status(400).json({ msg: "Mật khẩu không chính xác" });

      const accessToken = createAccessToken({ id: user._id });
      const refreshToken = createRefreshToken({ id: user._id });

      res.cookie("refreshtoken", refreshToken, {
        httpOnly: true, //accessible only by web server
        secure: true, //https
        sameSite: "None", //cross-site cookie
        maxAge: 30 * 7 * 24 * 60 * 60 * 1000,
      });

      res.json({ msg: "Đăng nhập thành công.", accessToken, user: { ...user._doc, password: "" } });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },

  logout: async (req, res) => {
    try {
      res.clearCookie("refreshtoken", {
        httpOnly: true,
        sameSite: "None",
        secure: true,
      });
      return res.json({ msg: "Đăng xuất thành công." });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },

  generateAccessToken: async (req, res) => {
    try {
      const rf_token = req.cookies.refreshtoken;
      if (!rf_token) return res.status(400).json({ msg: "Đăng nhập ngay bây giờ." });

      jwt.verify(rf_token, process.env.REFRESH_TOKEN, async (err, result) => {
        if (err) return res.status(400).json({ msg: "Đăng nhập ngay bây giờ." });

        const user = await User.findById(result.id)
          .select("-password")
          .populate("followers following", "-password");

        if (!user) return res.status(400).json({ msg: "Tài khoản này không tồn tại." });

        const accessToken = createAccessToken({ id: result.id });

        res.json({ accessToken, user });
      });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },

  searchUser: async (req, res) => {
    try {
      if (req.query.userName !== "") {
        const users = await User.find({ userName: { $regex: req.query.userName } })
          .limit(10)
          .select("name userName avatar");

        res.json({ users });
      }
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },

  getUser: async (req, res) => {
    try {
      const user = await User.findById(req.params.id)
        .select("-password")
        .populate("followers following", "-password");
      if (!user) return res.status(400).json({ msg: "Tài khoản không tồn tại." });

      res.json({ user });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },

  updateUser: async (req, res) => {
    try {
      const { _id, avatar, name, userName, email, gender, description } = req.body;

      const user = await User.findOneAndUpdate(
        { _id: _id },
        {
          avatar,
          name,
          userName,
          email,
          gender,
          description,
        }
      );

      res.json({
        msg: "Sửa thông tin người dùng thành công.",
        newUser: { ...user._doc, avatar, name, userName, email, gender, description },
      });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },

  updatePasswordUser: async (req, res) => {
    try {
      const { _id, oldPassword, newPassword } = req.body;

      const user = await User.findById(_id);

      if (!user) return res.status(400).json({ msg: "Tài khoản này không tồn tại." });

      const isMatchPassword = await bcrypt.compare(oldPassword, user.password);
      if (!isMatchPassword) return res.status(400).json({ msg: "Mật khẩu cũ không chính xác" });

      const newPasswordHash = await bcrypt.hash(newPassword, 12);

      await User.findOneAndUpdate(
        { _id: _id },
        {
          password: newPasswordHash,
        }
      );

      res.json({ msg: "Thay đổi mật khẩu thành công." });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },

  follow: async (req, res) => {
    try {
      const user = await User.find({ _id: req.body.idUser, followers: req.body.idUserFollower });
      if (user.length > 0) return res.status(400).json({ msg: "Bạn đã theo dõi tài khoản này rồi." });

      const newUser = await User.findOneAndUpdate(
        { _id: req.body.idUser },
        {
          $push: { followers: req.body.idUserFollower },
        },
        { new: true }
      ).populate("followers following", "-password");

      await User.findOneAndUpdate(
        { _id: req.body.idUserFollower },
        {
          $push: { following: req.body.idUser },
        },
        { new: true }
      );

      res.json({ msg: "Theo dõi thành công", newUser });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },

  unfollow: async (req, res) => {
    try {
      const newUser = await User.findOneAndUpdate(
        { _id: req.body.idUser },
        {
          $pull: { followers: req.body.idUserFollower },
        },
        { new: true }
      ).populate("followers following", "-password");

      await User.findOneAndUpdate(
        { _id: req.body.idUserFollower },
        {
          $pull: { following: req.body.idUser },
        },
        { new: true }
      );

      res.json({ msg: "Bỏ theo dõi thành công", newUser });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },

  suggestionUser: async (req, res) => {
    try {
      const user = await User.findById(req.params.id);
      if (!user) return res.status(400).json({ msg: "Người dùng không tồn tại." });

      const newArr = [...user.following, user._id];
      const num = req.query.num || 10;

      const userSuggestion = await User.aggregate([
        { $match: { _id: { $nin: newArr } } },
        { $sample: { size: num } },
        { $lookup: { from: "users", localField: "followers", foreignField: "_id", as: "followers" } },
        { $lookup: { from: "users", localField: "following", foreignField: "_id", as: "following" } },
      ]).project("-password");

      res.json({ userSuggestion });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },
};

const createAccessToken = (payload) => {
  return jwt.sign(payload, process.env.ACCESS_TOKEN, { expiresIn: "1d" });
};

const createRefreshToken = (payload) => {
  return jwt.sign(payload, process.env.REFRESH_TOKEN, { expiresIn: "30d" });
};

module.exports = userController;
