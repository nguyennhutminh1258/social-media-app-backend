const Comment = require("../models/commentModel");
const Post = require("../models/postModel");

const commentController = {
  createComment: async (req, res) => {
    try {
      const { idUser, idUserPost, idPost, content, tag, reply } = req.body;

      const post = await Post.findById(idPost);
      if (!post)
        return res.status(400).json({ msg: "Bài viết này không tồn tại." });

      if (reply) {
        const comment = await Comment.findById(reply);
        if (!comment)
          return res.status(400).json({ msg: "Bình luận này không tồn tại." });
      }

      const newComment = new Comment({
        user: idUser,
        content,
        tag,
        reply,
        idPost,
        idUserPost,
      });

      // Cập nhật bình luận vào bài viết
      await Post.findOneAndUpdate(
        { _id: idPost },
        { $push: { comments: newComment._id } },
        { new: true }
      );

      await newComment.save();

      res.json({ msg: "Bình luận thành công", newComment });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  updateComment: async (req, res) => {
    try {
      const { idComment, idUser, content } = req.body;

      await Comment.findOneAndUpdate(
        { _id: idComment, user: idUser },
        { content }
      );

      res.json({ msg: "Sửa bình luận thành công" });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  likeComment: async (req, res) => {
    try {
      const { idUser } = req.body;

      const comment = await Comment.find({ _id: req.params.id, likes: idUser });
      if (comment.length !== 0) {
        return res.status(400).json({ msg: "Bạn đã thích bình luận này rồi." });
      }

      await Comment.findOneAndUpdate(
        { _id: req.params.id },
        {
          $push: { likes: idUser },
        },
        { new: true }
      );

      res.json({ msg: "Bạn đã thích bình luận này." });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  unLikeComment: async (req, res) => {
    try {
      const { idUser } = req.body;

      await Comment.findOneAndUpdate(
        { _id: req.params.id },
        {
          $pull: { likes: idUser },
        },
        { new: true }
      );

      res.json({ msg: "Bạn đã bỏ thích bình luận này." });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  deleteComment: async (req, res) => {
    try {
      const { idUser, idUserPost } = req.body;
      const comment = await Comment.findOneAndDelete({
        _id: req.params.id,
        $or: [{ user: idUser }, { idUserPost: idUserPost }],
      });

      await Post.findOneAndUpdate(
        { _id: comment.idPost },
        {
          $pull: { comments: req.params.id },
        }
      );

      res.json({ msg: "Xóa bình luận thành công." });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },
};

module.exports = commentController;
