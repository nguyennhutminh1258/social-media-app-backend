const Conversation = require("../models/conversationModel");
const Message = require("../models/messageModel");

const messageController = {
  createMessage: async (req, res) => {
    try {
      const { idUser, recipient, text, media } = req.body;
      if (!recipient || !text.trim()) return;

      const newConversation = await Conversation.findOneAndUpdate(
        {
          $or: [
            { recipients: [idUser, recipient] },
            { recipients: [recipient, idUser] },
          ],
        },
        { recipients: [idUser, recipient], text, media },
        { new: true, upsert: true }
      );

      const newMessage = new Message({
        conversation: newConversation._id,
        sender: idUser,
        recipient,
        text,
        media,
      });

      await newMessage.save();

      res.json({ msg: "Tạo message thành công!" });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  getConversation: async (req, res) => {
    try {
      const conversations = await Conversation.find({
        recipients: req.params.id,
      })
        .sort("-updatedAt")
        .populate("recipients", "avatar userName name");

      res.json({ conversations });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  getMessages: async (req, res) => {
    try {
      const messages = await Message.find({
        $or: [
          { sender: req.params.sender, recipient: req.params.recipient },
          { sender: req.params.recipient, recipient: req.params.sender },
        ],
      });

      res.json({ messages });
    } catch (error) {
      res.status(500).json({ msg: error.message });
    }
  },

  deleteMessage: async (req, res) => {
    try {
      const { idSender, idRecipient } = req.body;

      await Message.findOneAndDelete({ _id: req.params.id, sender: idSender });

      // Lấy tin nhắn cuối trong conversation
      const lastMessages = await Message.find({
        $or: [
          { sender: idSender, recipient: idRecipient },
          { sender: idRecipient, recipient: idSender },
        ],
      })
        .sort({ _id: -1 })
        .limit(1);

      // Cập nhật text mới trong cuộc hội thoại
      await Conversation.findOneAndUpdate(
        { _id: lastMessages[0].conversation },
        { text: lastMessages[0].text },
        { new: true }
      );

      res.json({ msg: "Xóa message thành công!" });
    } catch (error) {}
  },
};

module.exports = messageController;
