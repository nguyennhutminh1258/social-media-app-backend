const Notify = require("../models/notifyModel");

const notifyController = {
  createNotify: async (req, res) => {
    try {
      const { id, recipients, url, text, content, image, user } = req.body;

      const newNotify = await new Notify({
        id,
        user,
        recipients,
        url,
        text,
        content,
        image,
      });

      await newNotify.save();

      return res.json({ newNotify });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },

  removeNotify: async (req, res) => {
    try {
      const notify = await Notify.findOneAndDelete({ id: req.params.id, url: req.query.url });
      return res.json({ notify });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },

  getNotifies: async (req, res) => {
    try {
      const notifies = await Notify.find({ recipients: req.params.id })
        .sort("isRead")
        .sort("-createdAt")
        .populate("user", "avatar userName");

      return res.json({ notifies });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },

  isRead: async (req, res) => {
    try {
      const notifies = await Notify.findOneAndUpdate({ _id: req.params.id }, { isRead: true });

      return res.json({ notifies });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },

  deleteAllNotifies: async (req, res) => {
    try {
      const notifies = await Notify.updateMany(
        { recipients: req.params.id },
        {
          $pull: { recipients: req.params.id },
        },
        { new: true }
      );

      return res.json({ notifies });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },
};

module.exports = notifyController;
